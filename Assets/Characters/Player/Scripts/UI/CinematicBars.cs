﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicBars : MonoBehaviour
{
    private static RectTransform topBar, bottomBar;
    private static int barHeight;

    // Start is called before the first frame update
    void Awake()
    {
        // Calculate bar height
        barHeight = (int)(GetComponentInParent<RectTransform>().rect.height * 0.25);

        // top bar
        GameObject gameObject = new GameObject("topBar", typeof(Image));
        gameObject.transform.SetParent(transform, false);
        gameObject.GetComponent<Image>().color = Color.black;
        
        topBar = gameObject.GetComponent<RectTransform>();
        topBar.anchorMin = new Vector2(0, 1);
        topBar.anchorMax = new Vector2(1, 1);
        topBar.sizeDelta = new Vector2(0, 0);

        // bottom bar
        gameObject = new GameObject("bottomBar", typeof(Image));
        gameObject.transform.SetParent(transform, false);
        gameObject.GetComponent<Image>().color = Color.black;

        bottomBar = gameObject.GetComponent<RectTransform>();
        bottomBar.anchorMin = new Vector2(0, 0);
        bottomBar.anchorMax = new Vector2(1, 0);
        bottomBar.sizeDelta = new Vector2(0, 0);
    }

    public static float SetBarDelta(float delta)
    {
        Vector2 sizeDelta = new Vector2(0, barHeight * delta);

        topBar.sizeDelta = sizeDelta;
        bottomBar.sizeDelta = sizeDelta;

        return delta;
    }

}
