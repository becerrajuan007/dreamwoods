﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	[SerializeField]
	public float CameraMoveSpeed = 120.0f;
	[SerializeField]
	public float clampAngle = 80.0f;
	[SerializeField]
	public float inputSensitivity = 150.0f;

	[SerializeField]
	public GameObject CameraFollowObj;
	[SerializeField]
	public GameObject CameraObj;
	[SerializeField]
	public GameObject PlayerObj;

	// Scripts
	private CombatHandler combatHandler;

	// Private
	private float rotY = 0.0f;
	private float rotX = 0.0f;

	private Camera ActualCameraObj;

	// ---------------------------------------------------------------------------------------------
	// Init
	// ---------------------------------------------------------------------------------------------
	void Awake()
	{
		combatHandler = transform.parent.gameObject.GetComponentInChildren<CombatHandler>();

		Vector3 rot = transform.localRotation.eulerAngles;
		rotY = rot.y;
		rotX = rot.x;

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		ActualCameraObj = CameraObj.GetComponent<Camera>();
	}

	// ---------------------------------------------------------------------------------------------
	// Public interface
	// ---------------------------------------------------------------------------------------------

	public void SetCameraRot(Quaternion newRot)
	{
		rotX = newRot.eulerAngles.x;
		rotY = newRot.eulerAngles.y;
	}

	public void UpdateCamera(Vector2 input)
	{
		if (combatHandler.isFocussed) return;

		rotY += input.x * inputSensitivity * Time.deltaTime;
		rotX += input.y * inputSensitivity * Time.deltaTime;

		rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

		Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
		transform.rotation = localRotation;
	}

	public void LateUpdateCamera()
	{
		// set the target object to follow
		Transform target = CameraFollowObj.transform;

		//move towards the game object that is the target
		float step = CameraMoveSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target.position, step);
	}

	// ---------------------------------------------------------------------------------------------
	// Getters
	// ---------------------------------------------------------------------------------------------
	public Camera GetCameraObject()
	{
		return ActualCameraObj;
	}

	public GameObject GetCameraBase()
	{
		return gameObject;
	}
}
