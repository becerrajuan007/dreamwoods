﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	[SerializeField]
	public float minDistance = 1.0f;
	[SerializeField]
	public float maxDistance = 4.0f;
	[SerializeField]
	public float smooth = 10.0f;

	// Private
	public float currentMaxDistance { get; set; }
	private Vector3 dollyDir;
	private float distance;

	// ---------------------------------------------------------------------------------------------
	// Init
	// ---------------------------------------------------------------------------------------------

	private void Awake () {
		currentMaxDistance = maxDistance;

		dollyDir = transform.localPosition.normalized;
		distance = transform.localPosition.magnitude;
	}

	// ---------------------------------------------------------------------------------------------
	// Public interface
	// ---------------------------------------------------------------------------------------------
	public float GetMaxDistance()
	{
		return maxDistance;
	}

	// ---------------------------------------------------------------------------------------------
	// Camera update
	// ---------------------------------------------------------------------------------------------

	// Update is called once per frame
	private void Update () {
		Vector3 desiredCameraPos = transform.parent.TransformPoint (dollyDir * currentMaxDistance);
		RaycastHit hit;

		if (Physics.Linecast (transform.parent.position, desiredCameraPos, out hit))
			distance = Mathf.Clamp ((hit.distance * 0.8f), minDistance, currentMaxDistance);
		else
			distance = currentMaxDistance;

		transform.localPosition = Vector3.Lerp (transform.localPosition, dollyDir * distance, Time.deltaTime * smooth);
	}
}
