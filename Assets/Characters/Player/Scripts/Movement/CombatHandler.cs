﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatHandler : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	// Scripts
	private PlayerController playerController;
	private AnimationHandler animationHandler;
	private InputHandler inputHandler;
	private CameraController cameraController;
	private CameraCollision cameraCollision;

	// ---------------------------------------------------------------------------------------------
	// Init
	// ---------------------------------------------------------------------------------------------

	private void Awake()
	{
		playerController = gameObject.GetComponent<PlayerController>();
		animationHandler = gameObject.GetComponent<AnimationHandler>();
		inputHandler = gameObject.GetComponent<InputHandler>();
		cameraController = transform.parent.gameObject.GetComponentInChildren<CameraController>();
		cameraCollision = transform.parent.gameObject.GetComponentInChildren<CameraCollision>();

		canDodge = true;
		dodgeLanded = true;
		dodgeState = DodgeState.NONE;
		focusDodged = false;

		canSwitchBack = true;
	}

	// ---------------------------------------------------------------------------------------------
	// Public interface
	// ---------------------------------------------------------------------------------------------

	// ---------------------
	// Dodge
	// ---------------------
	public enum DodgeState
	{
		NONE, FORWARD, BACKWARD, LEFT, RIGHT
	}
	public bool canDodge { get; private set; }
	public bool dodgeLanded { get; private set; }
	public DodgeState dodgeState { get; private set; }
	public bool focusDodged { get; private set; }
	public void Dodge(Vector2 input)
	{
		canDodge = false;
		dodgeLanded = false;

		if (isFocussed)
		{
			focusDodged = true;
			if (Mathf.Abs(inputHandler.GetVertInput()) <= 0.1f && Mathf.Abs(inputHandler.GetHorInput()) <= 0.1f)
			{
				// Neutral dodge backwards
				animationHandler.TriggerBDodgeAnimation();
				dodgeState = DodgeState.BACKWARD;
			}
			else if (inputHandler.GetVertInput() >= 0)
			{
				if (inputHandler.GetHorInput() >= 0)
				{
					// UP RIGHT
					if (inputHandler.GetVertInput() > inputHandler.GetHorInput())
					{
						// UP
						animationHandler.TriggerFDodgeAnimation();
						dodgeState = DodgeState.FORWARD;
					}
					else
					{
						// RIGHT
						animationHandler.TriggerRDodgeAnimation();
						dodgeState = DodgeState.RIGHT;
					}
				}
				else
				{
					// UP LEFT
					if (inputHandler.GetVertInput() > -inputHandler.GetHorInput())
					{
						// UP
						animationHandler.TriggerFDodgeAnimation();
						dodgeState = DodgeState.FORWARD;
					}
					else
					{
						// LEFT
						animationHandler.TriggerLDodgeAnimation();
						dodgeState = DodgeState.LEFT;
					}
				}
			}
			else
			{
				if (inputHandler.GetHorInput() >= 0)
				{
					// DOWN RIGHT
					if (-inputHandler.GetVertInput() > inputHandler.GetHorInput())
					{
						// DOWN
						animationHandler.TriggerBDodgeAnimation();
						dodgeState = DodgeState.BACKWARD;
					}
					else
					{
						// RIGHT
						animationHandler.TriggerRDodgeAnimation();
						dodgeState = DodgeState.RIGHT;
					}
				}
				else
				{
					// DOWN LEFT
					if (-inputHandler.GetVertInput() > -inputHandler.GetHorInput())
					{
						// DOWN
						animationHandler.TriggerBDodgeAnimation();
						dodgeState = DodgeState.BACKWARD;
					}
					else
					{
						// LEFT
						animationHandler.TriggerLDodgeAnimation();
						dodgeState = DodgeState.LEFT;
					}
				}
			}

		}
		else
		{
			if (Mathf.Abs(inputHandler.GetVertInput()) <= 0.1f && Mathf.Abs(inputHandler.GetHorInput()) <= 0.1f)
			{
				// Neutral dodge backwards
				animationHandler.TriggerBDodgeAnimation();
				dodgeState = DodgeState.BACKWARD;
			}
			else
			{
				// Dodge in the current direction
				animationHandler.TriggerFDodgeAnimation();
				dodgeState = DodgeState.FORWARD;
			}
		}
	}

	// ---------------------
	// Focus
	// ---------------------
	public bool isFocussed { get; private set; }
	private Quaternion targetRot;
	private float targetZoom;
	private float prevBarDelta = 0f;
	public void Focus(bool isHeld)
	{
		if (isHeld)
		{
			isFocussed = true;

			// Lerp to front rotation
			targetRot = Quaternion.LookRotation(transform.forward - new Vector3(0f, 0.2f, 0f), transform.up);
			cameraController.GetCameraBase().transform.rotation = Quaternion.Slerp(cameraController.GetCameraBase().transform.rotation, targetRot, 0.5f);

			// Zoom in
			targetZoom = cameraCollision.GetMaxDistance() * 0.75f;
			cameraCollision.currentMaxDistance = Mathf.Lerp(cameraCollision.currentMaxDistance, targetZoom, 0.5f);

			// Get cinematic bars in
			prevBarDelta = CinematicBars.SetBarDelta(Mathf.Lerp(prevBarDelta, 1f, 0.5f));

		}
		else if (isFocussed)
		{
			isFocussed = false;

			// Set rotation to current rotation
			cameraController.SetCameraRot(cameraController.GetCameraBase().transform.rotation);
			cameraCollision.currentMaxDistance = cameraCollision.GetMaxDistance();
		}
		else
		{
			// Zoom out
			targetZoom = cameraCollision.GetMaxDistance();
			cameraCollision.currentMaxDistance = Mathf.Lerp(cameraCollision.currentMaxDistance, targetZoom, 0.5f);

			// Get cinematic bars out
			prevBarDelta = CinematicBars.SetBarDelta(Mathf.Lerp(prevBarDelta, 0f, 0.5f));
		}
	}

	// ---------------------
	// Draw weapon
	// ---------------------
	public bool hasMeleeDrawn { get; set; }
	public bool canSwitchBack { get; set; }

	public void DrawMelee()
	{
		canSwitchBack = false;
		hasMeleeDrawn = !hasMeleeDrawn;
	}


	// ---------------------
	// Light attack
	// ---------------------
	enum LightAttackState
	{
		IDLE, ATTACK_1, BUFFER_1, ATTACK_2, BUFFER_2, ATTACK_3, BUFFER_3
	}

	private LightAttackState lightAttackState = LightAttackState.IDLE;
	public void LightAttack()
	{
		if (!hasMeleeDrawn)
		{
			hasMeleeDrawn = true;
		}
		else
		{
			switch(lightAttackState)
			{
				case LightAttackState.IDLE:
					LightAttackC1();
					break;
				case LightAttackState.BUFFER_1:
					LightAttackC2();
					break;
				case LightAttackState.BUFFER_2:
					LightAttackC3();
					break;
			}
		}
	}


	private void LightAttackC1()
	{
		lightAttackState = LightAttackState.ATTACK_1;
		animationHandler.TriggerAttack1C1();
	}

	private void LightAttackC2()
	{
		lightAttackState = LightAttackState.ATTACK_2;
		animationHandler.TriggerAttack1C2();
	}

	private void LightAttackC3()
	{
		lightAttackState = LightAttackState.ATTACK_3;
		animationHandler.TriggerAttack1C3();
	}

	/*
	private bool attack2Toggle = false;
	public void Attack2(bool isHeld)
	{
		if (isHeld && !attack2Pressed)
		{
			attack2Pressed = true;

			// TODO - Do melee
			if (!hasMeleeDrawn)
			{
				hasMeleeDrawn = true;
			}
			else
			{
				attack2Toggle = true;
			}

		}
		else if (!isHeld && attack2Pressed)
		{
			attack2Pressed = false;
		}
	}
	*/

	// ---------------------------------------------------------
	// Combo system
	// ---------------------------------------------------------


	/*
	public float maxComboDelay = 0.9f;
	public float restTime = 0.5f;

	private int numHits = 0;
	private float lastHitTime = 0f;
	private float lastRestTime = 0f;

	public void UpdateComboSystem()
	{
		if (Time.time - lastHitTime > maxComboDelay || IsRecovering())
		{
			numHits = 0;
		}

		if (lightAttackToggle)
		{
			lightAttackToggle = false;

			lastHitTime = Time.time;
			numHits++;
			if (numHits == 1)
			{
				animationHandler.SetAttack1C1(true);
			}
			numHits = Mathf.Clamp(numHits, 0, 3);
		}

		// TODO
		//if (attack2Toggle)
		//{
		//
		//}
		
	}
	

	private bool IsRecovering()
	{
		return (Time.time - lastRestTime) <= restTime;
	}

	public void LightAttackCombo2()
	{
		if (numHits >= 2)
		{
			animationHandler.SetAttack1C2(true);
		}
		else
		{
			animationHandler.SetAttack1C1(false);

			numHits = 0;
			lastRestTime = Time.time;
		}
	}

	public void LightAttackCombo3()
	{
		if (numHits >= 3)
		{
			animationHandler.SetAttack1C3(true);
		}
		else
		{
			animationHandler.SetAttack1C1(false);
			animationHandler.SetAttack1C2(false);

			numHits = 0;
			lastRestTime = Time.time;
		}
	}

	private void LightAttackComboReset()
	{
		animationHandler.SetAttack1C1(false);
		animationHandler.SetAttack1C2(false);
		animationHandler.SetAttack1C3(false);

		numHits = 0;
		lastRestTime = Time.time;
	}
	*/

	// ---------------------------------------------------------------------------------------------
	// Animation keyframes
	// ---------------------------------------------------------------------------------------------

	public void LandDodge()
	{
		dodgeLanded = true;
	}

	public void EndDodge()
	{
		canDodge = true;
		focusDodged = false;
		dodgeState = DodgeState.NONE;
	}

	public void LightAttackNextState()
	{
		switch (lightAttackState)
		{
			case LightAttackState.ATTACK_1:
				lightAttackState = LightAttackState.BUFFER_1;
				break;
			case LightAttackState.ATTACK_2:
				lightAttackState = LightAttackState.BUFFER_2;
				break;
			case LightAttackState.ATTACK_3:
				lightAttackState = LightAttackState.BUFFER_3;
				break;
		}
	}

	public void ReturnToIdle()
	{
		lightAttackState = LightAttackState.IDLE;
	}
}
