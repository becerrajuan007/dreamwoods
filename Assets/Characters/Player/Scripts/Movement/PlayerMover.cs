﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	// Public
	[SerializeField]
	public float runSpeed = 6;
	[SerializeField]
	public float speedSmoothTime = 0.1f;
	[SerializeField]
	public float turnSmoothTime = 0.15f;
	[SerializeField]
	public float gravity = -12;
	[Range(0, 1)]
	public float airControlPercent;
	[SerializeField]
	public float slopeForce = 10f;

	// Private
	private Vector3 velocity;
	private float speedSmoothVelocity;
	private float turnSmoothVelocity;

	// Scripts
	private PlayerController playerController;
	private CombatHandler combatHandler;
	private CameraController cameraController;

	// Components
    private CharacterController controller;

    // ---------------------------------------------------------------------------------------------
    // Init
    // ---------------------------------------------------------------------------------------------

    private void Awake()
    {
		playerController = gameObject.GetComponent<PlayerController>();
		combatHandler = gameObject.GetComponent<CombatHandler>();
		cameraController = transform.parent.gameObject.GetComponentInChildren<CameraController>();

        controller = GetComponent<CharacterController>();
    }

    // ---------------------------------------------------------------------------------------------
    // Public interface
    // ---------------------------------------------------------------------------------------------

    public void MoveCharacter(Vector2 input)
    {
		// Update rotation based on input?
		if (combatHandler.canDodge && input != Vector2.zero)
		{
			// Calculate target rotation
			float targetRotation = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + cameraController.GetCameraObject().transform.eulerAngles.y;
			float targetTurnSmoothTime;

			// Turn faster if the player is not moving. Otherwise, turn slowly
			if (playerController.currentSpeed > 0.02)
				targetTurnSmoothTime = turnSmoothTime;
			else
				targetTurnSmoothTime = turnSmoothTime / 5;

			// Apply rotation
			if (!combatHandler.isFocussed)
				transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, GetModifiedSmoothTime(targetTurnSmoothTime));
		}

		// Set velocity based on input, taking acount the dodge states
		if (combatHandler.canDodge)
		{
			// Normal movement speed
			playerController.currentSpeed = Mathf.SmoothDamp(playerController.currentSpeed, runSpeed * input.magnitude, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));
		}
		else
		{
			if (combatHandler.dodgeLanded)
			{
				// Dodge start speed
				playerController.currentSpeed = runSpeed * 0.3f;
			}
			else
			{
				// Dodge end speed
				playerController.currentSpeed = runSpeed * 2;
			}
		}

		// Update downwards velocity based on gravity
		velocity.y += Time.deltaTime * gravity;

		if (combatHandler.isFocussed || combatHandler.focusDodged)
		{
			// CURRENTLY FOCUSED

			switch (combatHandler.dodgeState)
			{
				case CombatHandler.DodgeState.NONE:
					// Calc x and z input velocity
					float velocityX = input.y * runSpeed * 0.75f;
					float velocityZ = input.x * runSpeed * 0.75f;

					// Move based on velocity vector
					velocity = transform.forward * velocityX + Vector3.up * velocity.y + transform.right * velocityZ;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.FORWARD:

					// Move based on velocity vector
					velocity = transform.forward * playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.BACKWARD:

					// Move based on velocity vector
					velocity = transform.forward * -playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.RIGHT:

					// Move based on velocity vector
					velocity = transform.right * playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.LEFT:

					// Move based on velocity vector
					velocity = transform.right * -playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
			}
		}
		else
		{
			// CURRENTLY NOT FOCUSED

			switch (combatHandler.dodgeState)
			{
				case CombatHandler.DodgeState.NONE:

					// Move based on velocity vector
					velocity = transform.forward * playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.FORWARD:

					// Move based on velocity vector
					velocity = transform.forward * playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
				case CombatHandler.DodgeState.BACKWARD:

					// Move based on velocity vector
					velocity = transform.forward * -playerController.currentSpeed + Vector3.up * velocity.y;
					controller.Move(velocity * Time.deltaTime);

					break;
			}

		}

		// Move down if on a slope
		if (CorrectGrounded())
		{
			controller.Move(Vector3.down * controller.height / 2 * slopeForce * Time.deltaTime);
		}

		// Trigger landing state
		if (controller.isGrounded)
		{
			velocity.y = Time.deltaTime * gravity;
		}
	}

	// ---------------------------------------------------------------------------------------------
	// Getters
	// ---------------------------------------------------------------------------------------------

	public bool GetIsGrounded()
	{
		return controller.isGrounded;
	}

	public float GetNormalizedVelocityY()
	{
		// Normalize Y velocity for the jump animator
		if (velocity.y >= 0)
		{
			return 0.5f + (velocity.y / -gravity);
		}
		else
		{
			return velocity.y / gravity;
		}
	}

	// ---------------------------------------------------------------------------------------------
	// Utility
	// ---------------------------------------------------------------------------------------------

	private float GetModifiedSmoothTime(float smoothTime)
	{
		if (controller.isGrounded)
		{
			return smoothTime;
		}

		if (airControlPercent == 0)
		{
			return float.MaxValue;
		}
		return smoothTime / airControlPercent;
	}

	private bool CorrectGrounded()
	{
		// Raycast down, record hit data
		RaycastHit hitData;

		return Physics.Raycast(transform.position, Vector3.down, out hitData, controller.height);
	}
}
