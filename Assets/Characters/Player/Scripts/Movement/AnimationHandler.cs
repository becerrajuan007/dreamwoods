﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	// Scripts
	private PlayerController playerController;
	private PlayerMover playerMover;
	private InputHandler inputHandler;
	private CombatHandler combatHandler;

	// Animation hashes
	private int NormalVelocityHash = Animator.StringToHash("NormalVelocity");
	private int ForwardVelocityHash = Animator.StringToHash("ForwardVelocity");
	private int RightVelocityHash = Animator.StringToHash("RightVelocity");
	private int JumpVelocityHash = Animator.StringToHash("JumpVelocity");

	private int FDodgeTriggerHash = Animator.StringToHash("FDodgeTrigger");
	private int BDodgeTriggerHash = Animator.StringToHash("BDodgeTrigger");
	private int LDodgeTriggerHash = Animator.StringToHash("LDodgeTrigger");
	private int RDodgeTriggerHash = Animator.StringToHash("RDodgeTrigger");

	private int IsGroundedHash = Animator.StringToHash("IsGrounded");
	private int HasMeleeDrawnHash = Animator.StringToHash("HasMeleeDrawn");
	private int IsFocusedHash = Animator.StringToHash("IsFocused");

	private int IsRecoveringHash = Animator.StringToHash("IsRecovering");
	private int LightAttackC1Hash = Animator.StringToHash("LightAttackC1");
	private int LightAttackC2Hash = Animator.StringToHash("LightAttackC2");
	private int LightAttackC3Hash = Animator.StringToHash("LightAttackC3");
	//private int Attack2Hash = Animator.StringToHash("Attack2");

	// Components
	private Animator animator;

	// ---------------------------------------------------------------------------------------------
	// Init
	// ---------------------------------------------------------------------------------------------

	private void Awake()
	{
		playerController = gameObject.GetComponent<PlayerController>();
		playerMover = gameObject.GetComponent<PlayerMover>();
		inputHandler = gameObject.GetComponent<InputHandler>();
		combatHandler = gameObject.GetComponent<CombatHandler>();

		animator = GetComponent<Animator>();
	}

	// ---------------------------------------------------------------------------------------------
	// Public interface
	// ---------------------------------------------------------------------------------------------

	public void UpdateAnimations()
	{
		// animator -> x/z velocity
		float normalVelocityPercent = transform.forward.magnitude * playerController.currentSpeed / playerMover.runSpeed;
		animator.SetFloat(NormalVelocityHash, normalVelocityPercent, playerMover.speedSmoothTime, Time.deltaTime);

		// animator -> forward veloctiy
		animator.SetFloat(ForwardVelocityHash, inputHandler.GetVertInput(), playerMover.speedSmoothTime, Time.deltaTime);

		// animator -> right velocity
		animator.SetFloat(RightVelocityHash, inputHandler.GetHorInput(), playerMover.speedSmoothTime, Time.deltaTime);

		// animator -> airborne velocity
		animator.SetFloat(JumpVelocityHash, playerMover.GetNormalizedVelocityY(), playerMover.speedSmoothTime, Time.deltaTime);

		// animator -> isGrounded?
		animator.SetBool(IsGroundedHash, playerMover.GetIsGrounded());

		// animator -> HasWeaponDrawn?
		animator.SetBool(HasMeleeDrawnHash, combatHandler.hasMeleeDrawn);

		// animator -> IsFocused?
		animator.SetBool(IsFocusedHash, combatHandler.isFocussed);

		// animator -> IsRecovering?
		animator.SetBool(IsRecoveringHash, false);
	}

	// Directional dodge triggers
	public void TriggerFDodgeAnimation() { animator.SetTrigger(FDodgeTriggerHash); }
	public void TriggerBDodgeAnimation() { animator.SetTrigger(BDodgeTriggerHash); }
	public void TriggerLDodgeAnimation() { animator.SetTrigger(LDodgeTriggerHash); }
	public void TriggerRDodgeAnimation() { animator.SetTrigger(RDodgeTriggerHash); }

	// Light attack triggers
	public void TriggerAttack1C1() { animator.SetTrigger(LightAttackC1Hash); }
	public void TriggerAttack1C2() { animator.SetTrigger(LightAttackC2Hash); }
	public void TriggerAttack1C3() { animator.SetTrigger(LightAttackC3Hash); }
}