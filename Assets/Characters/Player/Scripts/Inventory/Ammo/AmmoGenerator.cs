﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoGenerator : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire"))
        {
            Vector3 firePos = new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z);
            AmmoManager.SpawnAmmo(firePos, transform.rotation);
        }
    }
}
