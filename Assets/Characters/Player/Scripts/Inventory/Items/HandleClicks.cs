﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandleClicks : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Clicked " + eventData.pointerPress.name);
    }
}
