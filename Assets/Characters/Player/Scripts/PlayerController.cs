﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class PlayerController : MonoBehaviour
{
	// ---------------------------------------------------------------------------------------------
	// Member vars
	// ---------------------------------------------------------------------------------------------

	// Scripts
	private InputHandler inputHandler;
	private CameraController cameraController;
	private AnimationHandler animationHandler;
	private CombatHandler combatHandler;

	// Basic movement
	public float currentSpeed { get; set; }

	// Weapon variables
	public GameObject undrawnSword;
	public GameObject undrawnShield;
	public GameObject drawnSword;
	public GameObject drawnShield;

	// ---------------------------------------------------------------------------------------------
	// Init
	// ---------------------------------------------------------------------------------------------

	private void Awake()
	{
		// Init scripts
		inputHandler = gameObject.GetComponent<InputHandler>();
		cameraController = transform.parent.gameObject.GetComponentInChildren<CameraController>();
		animationHandler = gameObject.GetComponent<AnimationHandler>();
		combatHandler = gameObject.GetComponent<CombatHandler>();

		drawnSword.SetActive(false);
		drawnShield.SetActive(false);
	}

	// ---------------------------------------------------------------------------------------------
	// Update
	// ---------------------------------------------------------------------------------------------

	private void Update()
	{
		inputHandler.HandleInputs();
		animationHandler.UpdateAnimations();
		//combatHandler.UpdateComboSystem();
	}

	private void LateUpdate()
	{
		cameraController.LateUpdateCamera();
	}

	// ---------------------------------------------------------------------------------------------
	// Animation keyframes
	// ---------------------------------------------------------------------------------------------

	public void SwapMeleeWeaponModels()
	{
		// undrawn weapons
		undrawnShield.SetActive(!combatHandler.hasMeleeDrawn);
		undrawnSword.SetActive(!combatHandler.hasMeleeDrawn);

		// drawn weapons
		drawnShield.SetActive(combatHandler.hasMeleeDrawn);
		drawnSword.SetActive(combatHandler.hasMeleeDrawn);

		combatHandler.canSwitchBack = true;
	}

}
