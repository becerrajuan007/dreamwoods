﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    // ---------------------------------------------------------------------------------------------
    // Member vars
    // ---------------------------------------------------------------------------------------------

    // Const input codes

    // Left stick
    private const string LEFT_STICK_HOR = "LeftStickHorizontal";
    private const string LEFT_STICK_VERT = "LeftStickVertical";
    private const string LEFT_STICK_BUTTON = "LeftStickButton";

    // Right stick
    private const string RIGHT_STICK_HOR = "RightStickHorizontal";
    private const string RIGHT_STICK_VERT = "RightStickVertical";
    private const string RIGHT_STICK_BUTTON = "RightStickButton";

    // Face buttons
    private const string A_BUTTON = "A";
    private const string B_BUTTON = "B";
    private const string X_BUTTON = "X";
    private const string Y_BUTTON = "Y";

    // Control pad
    private const string CONTROL_PAD_HOR = "ControlPadHorizontal";
    private const string CONTROL_PAD_VERT = "ControlPadVertical";

    // Bumpers
    private const string LEFT_BUMPER = "LB";
    private const string RIGHT_BUMPER = "RB";

    // Triggers
    private const string LEFT_TRIGGER = "LT";
    private const string RIGHT_TRIGGER = "RT";

    // Front buttons
    private const string START_BUTTON = "Start";
    private const string SELECT_BUTTON = "Select";

    // Scripts
    private CameraController cameraController;
    private PlayerMover playerMover;
    private CombatHandler combatHandler;

    // ---------------------------------------------------------------------------------------------
    // Init
    // ---------------------------------------------------------------------------------------------

    private void Awake()
    {
        cameraController = transform.parent.gameObject.GetComponentInChildren<CameraController>();
        playerMover = gameObject.GetComponent<PlayerMover>();
        combatHandler = gameObject.GetComponent<CombatHandler>();
    }

    // ---------------------------------------------------------------------------------------------
    // Public interface
    // ---------------------------------------------------------------------------------------------

    public void HandleInputs()
    {
        // USED INPUTS

        // Handle basic movement (Left stick)
        Move(new Vector2(Input.GetAxisRaw(LEFT_STICK_HOR), Input.GetAxisRaw(LEFT_STICK_VERT)));

        // Handle camera controls (Right stick)
        Look(new Vector2(Input.GetAxisRaw(RIGHT_STICK_HOR), Input.GetAxisRaw(RIGHT_STICK_VERT)));

        // Interact (A)
        Interact(Input.GetAxisRaw(A_BUTTON) > 0f);

        // Dodge (B)
        Dodge(Input.GetAxisRaw(B_BUTTON) > 0f);

        // Light attack (X)
        LightAttack(Input.GetAxisRaw(X_BUTTON) > 0f);

        // Heavy attack (Y)
        HeavyAttack(Input.GetAxisRaw(Y_BUTTON) > 0f);

        // Draw melee (LB)
        DrawMelee(Input.GetAxisRaw(LEFT_BUMPER) > 0f);

        // Focus (LT)
        Focus(Input.GetAxisRaw(LEFT_TRIGGER) > 0.5f);

        // UNUSED INPUTS
        if (Input.GetAxisRaw(LEFT_STICK_BUTTON) != 0f)   PLACEHOLDER_LEFT_STICK_BUTTON();
        if (Input.GetAxisRaw(RIGHT_STICK_BUTTON) != 0f)  PLACEHOLDER_RIGHT_STICK_BUTTON();

        if (Input.GetAxisRaw(CONTROL_PAD_HOR) != 0f || Input.GetAxisRaw(CONTROL_PAD_VERT) != 0f)
            PLACEHOLDER_CONTROL_PAD(new Vector2(Input.GetAxisRaw(CONTROL_PAD_HOR), Input.GetAxisRaw(CONTROL_PAD_VERT)));
        
        if (Input.GetAxisRaw(RIGHT_BUMPER) != 0f)        PLACEHOLDER_RB();
        if (Input.GetAxisRaw(RIGHT_TRIGGER) != 0f)       PLACEHOLDER_RT();
        if (Input.GetAxisRaw(START_BUTTON) > 0f)        PLACEHOLDER_START();
        if (Input.GetAxisRaw(SELECT_BUTTON) > 0f)       PLACEHOLDER_SELECT();
    }

    // ---------------------------------------------------------------------------------------------
    // Getters
    // ---------------------------------------------------------------------------------------------

    public float GetHorInput()
    {
        return Input.GetAxisRaw(LEFT_STICK_HOR);
    }

    public float GetVertInput()
    {
        return Input.GetAxisRaw(LEFT_STICK_VERT);
    }

    // ---------------------------------------------------------------------------------------------
    // Input methods
    // ---------------------------------------------------------------------------------------------

    // Left stick -> Read axis
    private void Move(Vector2 input)
    {
        playerMover.MoveCharacter(input);
    }

    // Left stick button (UNUSED)
    private void PLACEHOLDER_LEFT_STICK_BUTTON()
    {
        Debug.Log("LEFT STICK BUTTON");
    }

    // Right stick -> Read axis
    private void Look(Vector2 input)
    {
        cameraController.UpdateCamera(input);
    }

    // Right stick button (UNUSED)
    private void PLACEHOLDER_RIGHT_STICK_BUTTON()
    {
        Debug.Log("RIGHT STICK BUTTON");
    }

    // A -> Toggle
    private bool interactPressed = false;
    private void Interact(bool isHeld)
    {
        // Pressed
        if (isHeld && !interactPressed)
        {
            interactPressed = true;
            Debug.Log("Interact");
        }
        // Released
        else if (!isHeld && interactPressed)
        {
            interactPressed = false;
        }
    }

    // B -> Toggle
    private bool dodgePressed = false;
    private void Dodge(bool isHeld)
    {
        // Pressed
        if (playerMover.GetIsGrounded() && isHeld && !dodgePressed && combatHandler.canDodge)
        {
            dodgePressed = true;
            combatHandler.Dodge(new Vector2(Input.GetAxisRaw(LEFT_STICK_HOR), Input.GetAxisRaw(LEFT_STICK_VERT)));
        }
        // Released
        else if (!isHeld && dodgePressed)
        {
            dodgePressed = false;
        }
    }

    // X -> Toggle
    private bool lightAttackPressed = false;
    private void LightAttack(bool isHeld)
    {
        if (isHeld && !lightAttackPressed)
        {
            lightAttackPressed = true;
            combatHandler.LightAttack();
        }
        else if (!isHeld && lightAttackPressed)
        {
            lightAttackPressed = false;
        }
    }

    // Y -> Toggle
    private void HeavyAttack(bool isHeld)
    {
        // TODO
    }

    // Control pad (UNUSED)
    private void PLACEHOLDER_CONTROL_PAD(Vector2 input)
    {
        Debug.Log("CONTROL PAD - " + input);
    }

    // LB -> Toggle
    private bool drawMeleePressed = false;
    private void DrawMelee(bool isHeld)
    {
        if (playerMover.GetIsGrounded() && !drawMeleePressed && isHeld && combatHandler.canSwitchBack)
        {
            drawMeleePressed = true;
            combatHandler.DrawMelee();
        }
        else if (!isHeld && drawMeleePressed)
        {
            drawMeleePressed = false;
        }
    }

    // RB (UNUSED)
    private void PLACEHOLDER_RB()
    {
        Debug.Log("RB");
    }

    // LT -> Hold
    private void Focus(bool isHeld)
    {
        combatHandler.Focus(isHeld);
    }

    // RT (UNUSED)
    private void PLACEHOLDER_RT()
    {
        Debug.Log("RT");
    }

    // Start (UNUSED)
    private void PLACEHOLDER_START()
    {
        Debug.Log("Start");
    }

    // Select (UNUSED)
    private void PLACEHOLDER_SELECT()
    {
        Debug.Log("Select");
    }
}
