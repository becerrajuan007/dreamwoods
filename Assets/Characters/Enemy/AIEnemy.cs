﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIEnemy : MonoBehaviour
{
    public enum ENEMY_STATE { PATROL, CHASE, ATTACK};

    public ENEMY_STATE CurrentState
    {
        get { return currentState; }
        set
        {
            currentState = value;

            StopAllCoroutines();

            switch(currentState)
            {
                case ENEMY_STATE.PATROL:
                    StartCoroutine(AIPatrol());
                    break;
                case ENEMY_STATE.CHASE:
                    StartCoroutine(AIChase());
                    break;
                case ENEMY_STATE.ATTACK:
                    StartCoroutine(AIAttack());
                    break;
            }
        }
    }

    public IEnumerator AIPatrol ()
    {
        //Loop while patrolling
        while (currentState == ENEMY_STATE.PATROL)
        {
            //Set strict search
            ThisLineSight.Sensitity = LineSight.SightSensitivity.STRICT;

            //Chase to patrol position
            ThisAgent.isStopped = false;
            ThisAgent.SetDestination(PatrolDestination.position);

            //Wait until path is computed
            while (ThisAgent.pathPending)
                yield return null;

            //If we can see the target then start chasing
            if (ThisLineSight.CanSeeTarget)
            {
                ThisAgent.isStopped = true;
                CurrentState = ENEMY_STATE.CHASE;
                yield break;
            }

            //Wait until next frame
            yield return null;
        }
    }

    public IEnumerator AIChase()
    {
        while (currentState == ENEMY_STATE.CHASE)
        {
            //Set loose search
            ThisLineSight.Sensitity = LineSight.SightSensitivity.LOOSE;

            //Chase to last known position
            ThisAgent.isStopped = false;
            ThisAgent.SetDestination(ThisLineSight.LastKnowSighting);

            //Wait until path is computed
            while (ThisAgent.pathPending)
                yield return null;

            //Have we reached destination?
            if (ThisAgent.remainingDistance <= ThisAgent.stoppingDistance)
            {
                //Stop agent
                ThisAgent.isStopped = true;

                //Reached destination but cannot see player
                if (!ThisLineSight.CanSeeTarget)
                    CurrentState = ENEMY_STATE.PATROL;
                else //Reached destination and can see player. Reached attacking distance
                    CurrentState = ENEMY_STATE.ATTACK;

                yield break;
            }

            yield return null;
        }
    }

    public IEnumerator AIAttack()
    {
        //Loop while chasing and attacking
        while (currentState == ENEMY_STATE.ATTACK)
        {
            //Chase to player position
            ThisAgent.isStopped = false;
            ThisAgent.SetDestination(PlayerTransform.position);

            //Wait until path is computed
            while (ThisAgent.pathPending)
                yield return null;

            //Has player run away?
            if (ThisAgent.remainingDistance > ThisAgent.stoppingDistance)
            {
                //Change back to chase
                CurrentState = ENEMY_STATE.CHASE;
                yield break;
            }
            else
            {
                //Attack
                //PlayerHealth.HealthPoints -= MaxDamage * Time.deltaTime;
            }

            //Wait until next frame
            yield return null;
        }

        yield break;
    }

    [SerializeField]
    private ENEMY_STATE currentState = ENEMY_STATE.PATROL;

    //Reference to line of sight component
    private LineSight ThisLineSight = null;

    //Reference to nav mesh agent
    private NavMeshAgent ThisAgent = null;

    //Reference to transform
    private Transform ThisTransform = null;

    //Reference to player health
    //public Health PlayerHealth = null;

    //Reference to player transform
    private Transform PlayerTransform = null;

    //Reference to patrol destination
    public Transform PatrolDestination = null;

    //Damage amount per second
    public float MaxDamage = 10f;

    // Start is called before the first frame update
    void Awake()
    {
        ThisLineSight = GetComponent<LineSight>();
        ThisAgent = GetComponent<NavMeshAgent>();
        ThisTransform = GetComponent<Transform>();
        //PlayerTransform = PlayerHealth.GetComponent<Transform>();
    }
    //------------------------------------------
    void Start()
    {
        //Configure starting state
        CurrentState = ENEMY_STATE.PATROL;
    }
}
